package com.change;

import java.io.File;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.ComThread;
import com.jacob.com.Dispatch;

public class OfficeToHtml {

	public static final int WORD_HTML = 8;
	public static final int WORD_TXT = 7;
	public static final int EXCEL_HTML = 44;
	public static final int PPT_HTML = 44;

	public static void main(String[] args) {
		convert2HTML("I:\\使用方法.txt","I:\\使用方法.html");
	}

	/*
	 * 转换生存PDF文件，支持格式 doc docx txt xls xlsx 1、需安装office软件，并有将office转化为PDF的插件
	 * 2、需有jacob(java com bridge）， java与com组件之间的桥梁
	 */
	public static boolean convert2HTML(String inputFile, String pdfFile) {
		String suffix = getFileSufix(inputFile);
		File file = new File(inputFile);
		if (!file.exists()) {
			System.out.println("文件不存在！");
			return false;
		}
		OfficeToHtml officeToHtml = new OfficeToHtml();
		if (suffix.equals("doc") || suffix.equals("docx")
				|| suffix.equals("txt")) {
			return officeToHtml.word2HTML(inputFile, pdfFile);
		} else if (suffix.equals("xls") || suffix.equals("xlsx")) {
			return officeToHtml.excel2HTML(inputFile, pdfFile);
		} else {
			System.out.println("文件格式不支持转换!");
			return false;
		}
	}

	public static String getFileSufix(String fileName) {
		int splitIndex = fileName.lastIndexOf(".");
		return fileName.substring(splitIndex + 1);
	}

	/**
	 * WORD转HTML
	 * 
	 * @param docfile
	 *            WORD文件全路径
	 * @param htmlfile
	 *            转换后HTML存放路径
	 */
	public boolean word2HTML(String docfile, String htmlfile) {
		ActiveXComponent app = new ActiveXComponent("Word.Application"); // 启动word
		try {
			app.setProperty("Visible", false);
			app.setProperty("DisplayAlerts", false);// 设置不显示弹出覆盖警告
			Dispatch docs = app.getProperty("Documents").toDispatch();
			Dispatch doc = Dispatch.invoke(docs, "Open", Dispatch.Method,
					new Object[] { docfile, false, true }, new int[1])
					.toDispatch();
			Dispatch.invoke(doc, "SaveAs", Dispatch.Method, new Object[] {
					htmlfile, WORD_HTML }, new int[1]);
			Dispatch.call(doc, "Close", false);
			app.invoke("Quit", 0);
			return true;
		} catch (Exception e) {
			return false;
		} finally {
			ComThread.Release();
		}
	}

	/**
	 * EXCEL转HTML
	 * 
	 * @param xlsfile
	 *            EXCEL文件全路径
	 * @param htmlfile
	 *            转换后HTML存放路径
	 */
	public boolean excel2HTML(String xlsfile, String htmlfile) {
		ActiveXComponent app = new ActiveXComponent("Excel.Application"); // 启动exel
		try {
			app.setProperty("Visible", false);
			app.setProperty("DisplayAlerts", false);// 设置不显示弹出覆盖警告
			Dispatch excels = app.getProperty("Workbooks").toDispatch();
			Dispatch excel = Dispatch.invoke(excels, "Open", Dispatch.Method,
					new Object[] { xlsfile, false, true }, new int[1])
					.toDispatch();
			Dispatch.invoke(excel, "SaveAs", Dispatch.Method, new Object[] {
					htmlfile, EXCEL_HTML }, new int[1]);
			Dispatch.call(excel, "Close", false);
			app.invoke("Quit");
			return true;
		} catch (Exception e) {
			return false;
		} finally {
			ComThread.Release();
		}

	}
}
