1）于word、ppt等上传文件转换为PDF格式文件的环境搭建，步骤如下：
①	首先电脑要先安装office软件（不可以是WPS软件）
②	需要把jacob.dll文件复制到JDK的bin目录下面，否则无法调用转换为PDF的功能。

2）使用的服务器上必须安装有office软件,因为原理是调用office的pdf转换器来实现的。


3）必须也要有PDF软件，因为office要通过调用本地的pdf软件来实现格式的转换。

使用前操作
    1、把dll文件放在%JAVA_HOME%\bin下（注意系统是32位还是64位），也可以放在C:\Windows\System32下，如果是64位应该放在C:\Windows\SysWOW64 下。建议放在jdk的bin目录下
    2、如果是在eclipse下开发，需要重新引入jdk（Preference/Java/Installed JREs）
    3、开发时将jacab.jar包放在项目lib下并add到liabraries中即可。